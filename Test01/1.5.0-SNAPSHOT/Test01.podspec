Pod::Spec.new do |spec|
  spec.name         = "Test01"
  spec.version = "1.5.0-SNAPSHOT"
  spec.summary = "DANA SDK Base Library"

  spec.description  = <<-DESC
Base Library for Providing DANA SDK. 
                   DESC

  spec.homepage = "https://dana.id"
  spec.license = { :type => "MIT", :file => "LICENSE" }
  spec.author = "DANA Indonesia"
  spec.source = { :http => "https://nexus.dana.id/repository/ios-components/Test01/1.5.0-SNAPSHOT/Test01.framework.zip" } 

  spec.default_subspec = "Core" 
  spec.ios.deployment_target = "9.0" 

  spec.subspec "Core" do |core| 
   core.vendored_framework = "Test01.framework" 
   core.resources = "Test01.framework/*.bundle" 

   core.libraries =  "c++","xml2","bz2.1.0" 
   core.frameworks =  "MessageUI","CoreLocation","CoreMotion" 

   core.dependency "Mixpanel", "~> 3.6.0" 
   core.dependency "Split", "~> 2.6.0" 
   core.dependency "libwebp", "~> 1.1.0" 
   core.dependency "SocketRocket", "~> 0.5.1" 
   core.dependency "ZipArchive", "~> 1.4.0" 
  
  end 
  
  spec.subspec "KYC" do |plugin| 
   plugin.dependency "KYC", "~> 0.3.0" 
   plugin.dependency "Test01/Core" 
  end 
  
end
