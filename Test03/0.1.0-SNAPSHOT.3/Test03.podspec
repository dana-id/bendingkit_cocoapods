Pod::Spec.new do |spec|
  spec.name         = "Test03"
  spec.version = "0.1.0-SNAPSHOT.3"
  spec.summary = "DANA SDK Base Library"

  spec.description  = <<-DESC
Base Library for Providing DANA SDK. 
                   DESC

  spec.homepage = "https://dana.id"
  spec.license = { :type => "MIT", :file => "LICENSE" }
  spec.author = "DANA Indonesia"
  spec.source = { :http => "https://nexus.dana.id/repository/ios-components/Test03/0.1.0-SNAPSHOT.3/Test03.framework.zip" } 

  spec.default_subspec = "Core" 
  spec.ios.deployment_target = "9.0" 

  spec.subspec "Core" do |core| 
   core.vendored_framework = "Test03.framework" 
   core.resources = "Test03.framework/*.bundle" 

   core.libraries =  "c++","xml2","bz2.1.0" 
   core.frameworks =  "MessageUI","CoreLocation","CoreMotion" 

   core.dependency "Mixpanel", "~> 3.6.0" 
   core.dependency "Split", "~> 2.6.0" 
   core.dependency "libwebp", "~> 1.1.0" 
   core.dependency "SocketRocket", "~> 0.5.1" 
   core.dependency "ZipArchive", "~> 1.4.0" 
  
  end 
  
  spec.subspec "KYC" do |plugin| 
   plugin.dependency "KYC", "~> 0.3.0-SNAPSHOT.2" 
   plugin.dependency "Test03/Core" 
  end 
  
end
