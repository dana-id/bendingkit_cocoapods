#
# Be sure to run `pod lib lint DANAPaymentPlugin.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'PaymentQR'
  s.version = '0.3.2-SNAPSHOT.1'
  s.summary          = 'Base library for payment-related functionalities in DANA.'

  s.description      = <<-DESC
Base library for payment-related functionalities in DANA.
                       DESC

  s.homepage         = 'https://bitbucket.org/dana-id/airbender_paymentqr_ios/src'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Ricardo Pramana Suranta' => 'ricardo.pramana@dana.id' }
  s.source           = { :http => 'https://nexus.dana.id/repository/ios-components/PaymentQR/0.3.2-SNAPSHOT.1/PaymentQR.framework.zip'}

  s.ios.deployment_target = '9.0' 

  s.vendored_framework = 'PaymentQR.framework'
  s.resources = 'PaymentQR.framework/PaymentQR.bundle'

end
