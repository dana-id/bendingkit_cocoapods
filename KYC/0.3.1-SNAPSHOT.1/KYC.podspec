#
# Be sure to run `pod lib lint KYC.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'KYC'
  s.version = '0.3.1-SNAPSHOT.1'
  s.summary          = 'Component Library for KYC'

  s.description      = <<-DESC
  This library provide apply KYC for user
                       DESC

  s.homepage         = 'https://bitbucket.org/dana-id/kyc_component_ios'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Yosua Antonio Raphael Ekowidjaja' => 'yosua.ekowidjaja@dana.id' }
  s.source           = { :http => 'https://nexus.dana.id/repository/ios-components/KYC/0.3.1-SNAPSHOT.1/KYC.framework.zip' } 

  s.ios.deployment_target = '9.0'

  s.vendored_framework = 'KYC.framework'
  s.resources = 'KYC.framework/KYC.bundle'
  s.framework = 'AVFoundation','CoreMedia'
  
end
