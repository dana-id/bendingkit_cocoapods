#
#  Be sure to run `pod spec lint ScanQR.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|
  s.name             = 'ScanQR'
  s.version = '0.2.1-SNAPSHOT.2'
  s.summary          = 'Base library for providing Scan QR related functionalities.'

  s.description      = <<-DESC
Base library for providing Scan QR related functionalities.
                       DESC

  s.homepage         = 'https://bitbucket.org/dana-id/airbender_scanqr_ios/src/master/'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Muhammad Syah Royni' => 'syah.royni@dana.id' }
  s.source           = { :http => 'https://nexus.dana.id/repository/ios-components/ScanQR/0.2.1-SNAPSHOT.2/ScanQR.xcframework.zip' }  

  s.ios.deployment_target = '9.0'

  s.vendored_framework = 'ScanQR.xcframework' 
  s.resources = 'ScanQR.xcframework/ios-*/ScanQR.framework/ScanQR.bundle'

end
