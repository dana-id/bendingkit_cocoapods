Pod::Spec.new do |spec|
  spec.name    = 'DANAKit'
  spec.version = '0.1.0-SNAPSHOT.10'
  spec.summary = 'DANA SDK Base Library'

  spec.description  = <<-DESC
  Base Library for Providing DANA SDK. 
                   DESC

  spec.homepage = 'https://dana.id'
  spec.license = { :type => 'MIT', :file => 'LICENSE' }
  spec.author = 'DANA Indonesia'
  spec.source = { :http => 'https://nexus.dana.id/repository/ios-components/DANAKit/0.1.0-SNAPSHOT.10/DANAKit.xcframework.zip' } 

  spec.default_subspec = 'Core'
  spec.ios.deployment_target = '9.0'

  spec.pod_target_xcconfig = {
    'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
  }

  spec.user_target_xcconfig = {
    'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
  }

  spec.subspec 'Core' do |core| 
   core.vendored_framework = 'DANAKit.xcframework'
   core.resources = 'DANAKit.xcframework/ios-*/DANAKit.framework/*.{bundle}'

   core.libraries = 'c++','xml2','bz2.1.0'
   core.frameworks = 'MessageUI','CoreLocation','CoreMotion'

   core.dependency 'Mixpanel', '~> 3.6.0'
   core.dependency 'Split', '~> 2.11.0'
   core.dependency 'libwebp', '~> 1.1.0'
   core.dependency 'SocketRocket', '~> 0.5.1'
   core.dependency 'ZipArchive', '~> 1.4.0'
  end 

  spec.subspec 'KYC' do |plugin|
   plugin.dependency 'KYC', '~> 0.3.2-SNAPSHOT.1'
   plugin.dependency 'DANAKit/Core'
  end

  spec.subspec 'PaymentQR' do |plugin|
   plugin.dependency 'PaymentQR', '~> 0.3.3-SNAPSHOT.1'
   plugin.dependency 'DANAKit/Core'
  end

  spec.subspec 'ScanQR' do |plugin|
   plugin.dependency 'ScanQR', '~> 0.2.1-SNAPSHOT.1'
   plugin.dependency 'DANAKit/Core'
  end

end
