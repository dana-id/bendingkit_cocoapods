Pod::Spec.new do |spec|
  spec.name         = "DANAKit"
  spec.version = "0.1.0"
  spec.summary = "DANA SDK Base Library"

  spec.description  = <<-DESC
Base Library for Providing DANA SDK. 
                   DESC

  spec.homepage = "https://dana.id"
  spec.license = { :type => "MIT", :file => "LICENSE" }
  spec.author = "DANA Indonesia"
  spec.source = { :http => "https://nexus.dana.id/repository/ios-components/DANAKit/0.1.0/DANAKit.framework.zip" } 

  spec.ios.deployment_target = "9.0" 

  spec.vendored_framework = "DANAKit.framework" 
  spec.resources = "DANAKit.framework/*.bundle" 

  spec.libraries =  "c++","xml2","bz2.1.0" 
  spec.frameworks =  "MessageUI","CoreLocation","CoreMotion" 

  spec.dependency "Mixpanel", "~> 3.6.0" 
  spec.dependency "Split", "~> 2.5.3" 
  spec.dependency "libwebp", "~> 1.1.0" 
  spec.dependency "SocketRocket", "~> 0.5.1" 
  spec.dependency "ZipArchive", "~> 1.4.0" 
end